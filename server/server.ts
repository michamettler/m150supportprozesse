import { Type } from "@angular/compiler";

const express = require('express');
const app = express();

var fs = require('fs')
var morgan = require('morgan')
var path = require('path')

var fs = require("fs");

// setup logger
var accessLogStream = fs.createWriteStream(path.join(__dirname, '../log/access.log'), { flags: 'a' })
app.use(morgan('combined', { stream: accessLogStream }))

var review;

// setup express server
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content-Type, Accept');
    res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    } else {
        console.log('${req.ip} ${req.method} ${req.url}');
        next();
    }
});

app.use(express.json());

app.listen(4201, '127.0.0.1', function () {
    console.log('Sever now listening on 4201');
});

// get method for last created review for ticket (visbile on server (localhost:4201/reviews))
app.get('/reviews', (req, res) => {
    if (this.review) { //backend validation
        res.send([{
            object:
                "ticketId: " + this.review[0] +
                ", quality: " + this.review[1] +
                ", duration: " + this.review[2] +
                ", opinion: " + this.review[3]
        }])
    } else {
        res.send([{ message: "no saved reviews to display" }]);
    }
});

// post method to create new review for ticket
app.post('/reviews', (req, res) => {
        res.send({ body: req.body });

        this.review = [];
        this.review.push(req.body.review.ticketId);
        this.review.push(req.body.review.quality);
        this.review.push(req.body.review.duration);
        this.review.push(req.body.review.opinion);
    
        writeFile(this.review[0], this.review[1], this.review[2], this.review[3]);
});

function writeFile(ticketId: number, quality: number, duration: number, opinion: string) {

    var review = {
        ticketId: ticketId,
        quality: quality,
        duration: duration,
        opinion: opinion
    };

    fs.writeFile("./db/database.json", JSON.stringify(review, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        }
        console.log("File has been created");
    });
}

