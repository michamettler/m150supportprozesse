import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {MenuItem} from 'primeng/api';
import { HttpClient } from '@angular/common/http';
import { Ticket } from 'src/model/ticket';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    constructor(private http: HttpClient, private router: Router, private changeDetectorRefs: ChangeDetectorRef) { }
  
    items: MenuItem[];
    tickets: Array<Ticket> = [];

    ngOnInit() {
        this.items = [
            {
                label: 'Ticket-Übersicht'
            },
            {
                label: 'Einstellungen'
            },
        ];

        let ticket1: Ticket = {
            id: 1,
            validAt: new Date("2019-05-7"),
            description: "Maus defekt",
            status: true,
        }
        let ticket2: Ticket = {
            id: 2,
            validAt: new Date("2019-05-12"),
            description: "Internet Explorer hängt sich auf",
            status: true,
        }
        let ticket3: Ticket = {
            id: 3,
            validAt: new Date("2018-03-21"),
            description: "Badge verloren",
            status: false,
        }
        let ticket4: Ticket = {
            id: 4,
            validAt: new Date("2020-01-12"),
            description: "Monitor defekt",
            status: true,
        }

        this.tickets.push(ticket1);
        this.tickets.push(ticket2);
        this.tickets.push(ticket3);
        this.tickets.push(ticket4);
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [day, month, year].join('.');
    }

    closeTicket(ticket: Ticket) {
        localStorage.setItem("currentTicketId", ticket.id.toString());
        console.log("selected ticket: " + localStorage.getItem("currentTicketId"));
        this.router.navigate(['ticketReview']);

        this.tickets.slice(this.tickets.indexOf(ticket), 1);
        ticket.status = false;
  
        this.tickets = [...this.tickets, ticket]
    }

}
