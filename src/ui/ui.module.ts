import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { TicketReviewComponent } from './ticket-review/ticket-review.component';
import { RatingModule } from 'primeng/components/rating/rating';



@NgModule({
  declarations: [HomeComponent, TicketReviewComponent],
  imports: [
    CommonModule,
    RatingModule
  ]
})
export class UiModule { }
