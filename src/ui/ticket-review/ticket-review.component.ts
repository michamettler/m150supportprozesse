import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Review } from 'src/model/review';
import { AuthService } from 'src/auth.service';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-ticket-review',
  templateUrl: './ticket-review.component.html',
  styleUrls: ['./ticket-review.component.css']
})
export class TicketReviewComponent implements OnInit {

  review: Review;
  ticketId: string;

  constructor(private http: HttpClient, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.ticketId = localStorage.getItem("currentTicketId");

    this.review = {
      ticketId: parseInt(this.ticketId)
    }
  }

  back() {
    this.router.navigate(['home'])
  }

  submit() {
    this.auth.createReview(this.review);
  }

}
