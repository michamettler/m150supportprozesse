import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TicketReviewComponent } from 'src/ui/ticket-review/ticket-review.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { AngularWebStorageModule } from 'angular-web-storage';
import { StorageModule } from '@ngx-pwa/local-storage';
import { HomeComponent } from 'src/ui/home/home.component';
import { AuthGuard } from './auth.guard';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {MenubarModule} from 'primeng/menubar';
import {TableModule} from 'primeng/table';
import { RatingModule } from 'primeng/rating';
import {RadioButtonModule} from 'primeng/radiobutton';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ToggleButtonModule} from 'primeng/togglebutton';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TicketReviewComponent
  ],
  imports: [
    BrowserModule,
    RatingModule,
    HttpClientModule,
    ToggleButtonModule,
    AppRoutingModule,
    InputTextModule,
    RadioButtonModule,
    InputTextareaModule,
    TableModule,
    MenubarModule,
    ButtonModule,
    CommonModule,
    MessagesModule,
    MessageModule,
    FormsModule,
    CheckboxModule,
    AngularWebStorageModule,
    StorageModule.forRoot({ IDBNoWrap: true })
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
