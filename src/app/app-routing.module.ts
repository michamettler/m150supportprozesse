import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { HomeComponent } from 'src/ui/home/home.component';
import { AuthGuard } from './auth.guard'
import { TicketReviewComponent } from 'src/ui/ticket-review/ticket-review.component';

const routes: Routes = [
  { 
    path: '',
    component: HomeComponent },
  { 
    path: 'home', 
    component: HomeComponent },
  { 
    path: 'ticketReview', 
    component: TicketReviewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
