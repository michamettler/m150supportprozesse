import { Injectable } from '@angular/core';
import { Review } from './model/review';
import { HttpClient } from "@angular/common/http"
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedInStatus = false;

  constructor(private http: HttpClient) { }

  // store current review to local storage & save it on server
  createReview(review: Review) {

    localStorage.setItem(review.ticketId.toString(), JSON.stringify(review));

    this.http.post<any[]>('http://localhost:4201/reviews', { review })
      .subscribe(next => console.log(next));
  }
}
