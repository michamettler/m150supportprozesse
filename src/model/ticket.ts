export class Ticket {
    id: number;
    description?: string;
    validAt: Date;
    status: boolean;
  }