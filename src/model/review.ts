export class Review {
  ticketId: number;
  quality?: number;
  duration?: number;
  opinion?: string;
}